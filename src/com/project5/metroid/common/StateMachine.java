package com.project5.metroid.common;

import android.util.Log;

/* StateMachine is invoked by any class that extends BaseGameEntity.java
*  The State Machine manages the transition between action states and behaviors for a game entity
*/

public class StateMachine<Entity>
{
	private static final String TAG = "StateMachine";

	private Entity owner;			//agent that owns this instance

	private State currentState;	//Sets the starting state action to execute for this entity

	private State previousState;	//record of last state agent was in for reference

	private State globalState;		//global States; defines additional behaviors i.e., 'patrol mode' for now.

	public StateMachine(Entity owner)
	{
		this.owner = owner;
		currentState = null;
		previousState = null;
		globalState = null;
		
	}//end constructor
	
	public void setOwner(Entity owner)
	{
		this.owner=owner;
	}

	//Methods used to instantiate the Finite state machine
	public void SetCurrentState(State s)
	{
		currentState = s;
	}

	public void SetGlobalState(State s)
	{
		globalState = s;
	}

	void SetPreviousState(State s)
	{
		previousState = s;
	}

	//call this to update the FSM
	public void update()
	{	
		//possible global state update
		if(globalState != null)
			globalState.execute(owner);

		//regular state update
		if(currentState != null)
			currentState.execute(owner);
	}

	//change to a new state
	@SuppressWarnings("unchecked")
	public void changeState(State newState)
	{
		previousState = currentState;//keep a record of previous state

		//call exit method of the existing state
		previousState.exit(owner);
		
		currentState = newState;

		//change state to new state
		currentState.enter(owner);
	}

	//change state back to the previous state
	public void revertToPrevious()
	{
		changeState(previousState);
	}
	
	public boolean handleMessage(Telegram msg)
	{
		//If currentState is valid, see if the action state can handle the message
		if((currentState != null) && currentState.onMessage(owner, msg))
		{
			return true;
		}
		//else, if globalState is valid, see if it can handle the message instead
		else if((globalState != null) && globalState.onMessage(owner, msg))
		{
			return true;
		}
		return false;
	}

	//accessors---------------------------------------------------------------
	public StateMachine getStateMachine()
	{
		return this;
	}

	public State getCurrentState()
	{
		return currentState;
	}

	public State getGlobalState()
	{
		return globalState;
	}

	public State getPreviousState()
	{
		return previousState;
	}

	
	
	/*boolean isInState(State st)
	{
		if(currentState instanceof st)
			return true;
		else 
			return false;
	}*/

}//end StateMachine

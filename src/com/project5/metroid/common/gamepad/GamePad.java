package com.project5.metroid.common.gamepad;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;

import com.project5.metroid.R;
import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.MessageDispatcher;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;
import com.project5.metroid.common.Player.Player;

public class GamePad extends BaseGameEntity
{
	//Android debug 
	public static final String TAG = "GAMEPAD";
	
	boolean up,down,left,right;
	boolean a,b,select; 
	
	int id_dpad,id_AB;
	
	Paint paint;
	Context context;
	
	public GamePad(Context context)
	{	
		//State flags. True indicates a button is being touched currently.
		up = false;
		down=false;
		left=false;
		right=false;
		a = false;
		b = false;
		select = false;
		
		
		
		paint = new Paint();
		this.context = context;
		
	}
	
	
	public void draw(Canvas canvas)
	{	
		
		int color = Color.argb(175,200,200,200);
		paint.setColor(color);
		paint.setAntiAlias(true);
		
		//D-Pad graphic
		canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.nes_controller), 25,300, null);
		
		canvas.drawCircle(580,430, 50, paint);//B Button TEMP
		canvas.drawCircle(710,400,80,paint);  //A Button TEMP
	}
	
	public void update(Player player)
	{
		MessageDispatcher md = MessageDispatcher.getInstance();
		//We check flags thrown from event handler and dispatch the appropriate message
		//to the player 
		if(right)
		{
			if(up)			//Move right and aim up
			{
				Log.d(TAG,"Dispatching a move right/aim up movement");
				md.dispatch(0, this, player, message_type.MOVERIGHT_AIM_UP, 0);
			}
			else if(up && b)		//Move right and fire weapon
			{
				Log.d(TAG,"Dispatching a Moveright attack up movment");
				md.dispatch(0, this, player, message_type.MOVERIGHT_ATTACK_UP, 0);
			}
			else if(b)//Move right, aim up and fire
			{
				Log.d(TAG,"Dispatching a Move Right, Attack movement");
				md.dispatch(0, this, player, message_type.MOVERIGHT_ATTACK, 0);
			}
			else if(a)		//Jump to the right
				md.dispatch(0, this, player, message_type.JUMPRIGHT, 0);
			else			//Move right only
			{
				Log.d(TAG, "Dispatching a move right movement");
				md.dispatch(0, this, player, message_type.MOVERIGHT, 0);
			}
			
			
		}
		else if(left)
		{
			if(up)			//move left while aiming up
			{
				Log.d(TAG,"Dispatching a Move_left_aim_UP");
				md.dispatch(0, this, player, message_type.MOVELEFT_AIM_UP, 0);
			}
			else if(up && b)//move left and fire weapon upwards
			{
				Log.d(TAG,"Dispatching a Moveleft_attack_up");
				md.dispatch(0, this, player, message_type.MOVELEFT_ATTACK_UP, 0);
			}
			else if(b)		//Move left and fire weapon forwards
			{
				Log.d(TAG,"Dispatching a moveleft attack");
				md.dispatch(0, this, player, message_type.MOVELEFT_ATTACK, 0);
			}
			else if(a)		//Jump left
			{
				Log.d(TAG,"Dispatching a jump left");
				md.dispatch(0, this, player, message_type.JUMPLEFT, 0);
			}
			else			//Move left only
			{
				Log.d(TAG,"Dispatching a move left");
				md.dispatch(0, this, player, message_type.MOVELEFT, 0);
			}
		}
		else if(down)
		{
			//Switch to ball, if ability unlocked
			md.dispatch(0, this, player, message_type.DUCK, 0);
		}
		else if(up)
		{
			if(a)		//Jump straight up
				md.dispatch(0, this, player, message_type.JUMP, 0);
			else if(b)	//Fire straight up without moving
				md.dispatch(0, this, player, message_type.ATTACK_UP, 0);
			else		//Aim up, don't fire weapon
				md.dispatch(0, this, player, message_type.AIM_UP, 0);
		}
		else if(a)
		{	//Jump staright up only
			Log.d(TAG,"Dispatching jump command!");
			md.dispatch(0, this, player, message_type.JUMP, 0);
		}
		else if(b)
		{	//Fire weapon only
			md.dispatch(0, this, player, message_type.NOACTION,0);
			md.dispatch(0, this, player, message_type.ATTACK, 0);
		}
		else//No player input. 
			md.dispatch(0, this, player, message_type.NOACTION, 0);
	}
	
	/**
	 * TODO: Find better way of determining button presses than hard-coded pixel bounds
	 * 
	 * @param ACTION - enum, type of action commited
	 * @param f		 - canvas x coord of event
	 * @param g		 - canvas y coord of event
	 * @return
	 */
	public boolean handleInput(MotionEvent event)
	{
		//Debug
		Log.d(TAG, "Motion Event Pointer Count: "+event.getPointerCount());
		
		final int action = event.getAction();
		int x,y,id;
		
		//Multitouch Event handler
		switch(action & MotionEvent.ACTION_MASK)//This gives us the action code (int)
		{
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN:
		{
			
				id = event.getActionIndex();
				x = (int)event.getX(id);
				y = (int)event.getY(id);
				Log.d(TAG, "Entered Multitouch event handler with coords:("+x+","+y+")");
				
				Log.d(TAG,"Detected Action pointer down");
				//Locate D-Pad touch
				if(x < 200 && y > 295)
				{
					id_dpad = id;//store id for release later
					//decide which part of d-pad is being pressed
					if(x > 115)
					{
						Log.d(TAG,"Right button touched!");
						right = true;
					}
					if(y < 335)
					{
						Log.d(TAG,"Up button touched!");
						up = true;
					}
					if(y > 360)
					{
						Log.d(TAG,"Down Button touched!");
						down = true;
					}
					if(x < 65 )
					{
						Log.d(TAG,"Left Button touched");
						left = true;
					}
					
				}
				//Locate A/b Button touch
				else if(x > 540 && y > 250)
				{
					id_AB = id;//store id for release later
					
					Log.d(TAG,"Touched A/B Buttons! id: "+id_AB);
					if(x < 630 && y > 400)
					{
						Log.d(TAG, "Detected B button press!");
						b = true;
					}
						
					if(x > 630 && y > 300)
					{
						Log.d(TAG, "Detected A button press!");
						a = true;
					}
				}
				break;
		}//end case action_down/action_pointer_down
		
		case MotionEvent.ACTION_UP:
		case MotionEvent.ACTION_POINTER_UP:
		{
			id = event.getActionIndex();
			x = (int)event.getX(id);
			y = (int)event.getY(id);
			Log.d(TAG, "Entered Multitouch event handler with coords:("+x+","+y+")");
			/*
			if(id == id_dpad)
			{
				Log.d(TAG,"D-pad pointer id released: "+id_dpad);
				left = false;
				right = false;
				up = false;
				down = false;
			}
			else if(id == id_AB)
			{
				Log.d(TAG,"AB pointer id released: "+id_AB);
				a = false;
				b = false;
			}*/
			////////////////////////////////////////////////////////////////////////
			//Locate D-Pad touch
			if(x < 200 && y > 295)
			{
				id_dpad = id;//store id for release later
				right = false;
				left = false;
				up = false;
				down = false;
				//decide which part of d-pad is being pressed
				/*if(x > 115)
				{
					Log.d(TAG,"Right button released!");
					right = true;
				}
				if(y < 335)
				{newState
					Log.d(TAG,"Up button released!");
					up = true;
				}
				if(y > 360)
				{
					Log.d(TAG,"Down Button released!");
					down = true;
				}
				if(x < 65 )
				{
					Log.d(TAG,"Left Button released");
					left = true;
				}*/
				
			}
			//Locate A/b Button release
			else if(x > 540 && y > 250)
			{
				id_AB = id;//store id for release later
				
				Log.d(TAG,"Released A/B Buttons! id: "+id_AB);
				if(x < 630 && y > 400)
				{
					Log.d(TAG, "Detected B button release!");
					b = false;
				}
					
				if(x > 630 && y > 300)
				{
					Log.d(TAG, "Detected A button release!");
					a = false;
				}
			}
			
			break;
		}//End case action_up/action_pointer_up
		
		case MotionEvent.ACTION_MOVE:
		{
			Log.d(TAG,"Detected Move action");
			id = event.getActionIndex();
			x = (int)event.getX(id);
			y = (int)event.getY(id);
			
			//TODO: see about allowing button-mashing with A/B buttons
			
			//Check to see if player is button mashing the d-pad and adjust accordingly
			if(id == id_dpad && x < 200 && y > 295)
			{
				//decide which part of d-pad is being pressed
				if(x > 115)
				{
					Log.d(TAG,"Right button touched!");
					right = true;
					left = false;
				}
				if(y < 335)
				{
					Log.d(TAG,"Up button touched!");
					up = true;
					down = false;
				}
				if(y > 360)
				{
					Log.d(TAG,"Down Button touched!");
					down = true;
					up = false;
				}
				if(x < 65 )
				{
					Log.d(TAG,"Left Button touched");
					left = true;
					right = false;
				}
			}
		}//end case action_move
		
		};//End switch
		
		
		return true;
	}

	@Override
	public void HandleMessage(Telegram msg)
	{
		//TODO: Likely not going to utilize this function
		//Extension of BaseGameObject was required to facilitate message dispatching, not receiving
	}


	@Override
	public Bitmap draw()
	{
		// TODO Auto-generated method stub
		return null;
	}
}

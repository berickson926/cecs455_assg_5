package com.project5.metroid.common.powerup;

import android.graphics.Rect;
import android.util.Log;

import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.EntityManager;
import com.project5.metroid.common.MessageDispatcher;
import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;
import com.project5.metroid.common.Player.Player;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;


/**
 * Only available state a powerup can be in. 
 * Probably can have better naming for this...
 * 
 * @author bret
 *
 */
public class AvailablePowerUp extends State<BallUpgrade>
{
	//Android logcat debug
	public static final String TAG = "PowerUp";
	
	@Override
	public void enter(BallUpgrade E)
	{
		
	}

	@Override
	public void execute(BallUpgrade E)
	{
		//Check to see if the player has touched this power up
		//setup a rectangle to compare with the player for a less resource intensive collision detection
		E.powerUpRect = new Rect((int)E.position.x, (int)(E.position.y + E.spriteHeight/2), (int)E.position.x+E.spriteWidth, (int)E.position.y+E.spriteHeight);
				
		EntityManager em = EntityManager.getInstance();
		BaseGameEntity player = em.getEntityFromID(Player.playerID);
		Rect rect = new Rect((int)(player.position.x), 
							 (int)(player.position.y),
							 (int)(player.position.x+player.spriteWidth),
							 (int)(player.position.y+player.spriteHeight));
		
		if(Rect.intersects(rect, E.powerUpRect))
		{
			//Log.d(TAG,"PLAYER TOUCHED POWERUP");
			LevelManager lm = LevelManager.getInstance();
			TileMap map = lm.getLevel();
			
			//remove powerup from game
			map.removeGameObject(E);
			
			//dispatch message to player
			MessageDispatcher md = MessageDispatcher.getInstance();
			md.dispatch(0, E, em.getEntityFromID(Player.playerID), message_type.BALL_ACQUIRED, 0);
			
			//play fanfare music?
		}
		//else
		//	Log.d(TAG,"PLAYER HASNT TOUCHED POWERUP");
	}

	@Override
	public void exit(BallUpgrade E)
	{
		
	}

	@Override
	public boolean onMessage(BallUpgrade E, Telegram telegram)
	{
		// TODO Auto-generated method stub
		return false;
	}

}

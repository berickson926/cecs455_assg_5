package com.project5.metroid.common.powerup;

import com.project5.metroid.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.Spritesheet;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.Vector2D;

public class BallUpgrade extends BaseGameEntity
{
	private String 		name;		//Name of the powerup
	private	int			qty;  		//qty of this resource awarded to the player
	private Bitmap		bitmap;		//contains the full sprite sheet
	private Spritesheet	sprites;	//cut up sprite sheet for accesses individual frames
	public  Rect		powerUpRect;
	public 	Context		context;
	
	public BallUpgrade(Context context)
	{
		this.name = "Ball";
		this.context = context;
		//Setup sprites
		bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.m1samus_fixed);
		int width = (int) (27+27); //size adjustment, need to figure out better way
		int height= (int) (40+41); //to compensate for resolution change
		sprites= new Spritesheet(bitmap,width,height,9,12);
		sprites.setPlayingFrames(1, 3);
		sprites.setAnimSpeed(1);
		
		this.spriteWidth = sprites.getSprite(0).getWidth();
		this.spriteHeight= sprites.getSprite(0).getHeight();
		this.stateMachine.SetCurrentState(new AvailablePowerUp());
		this.stateMachine.changeState(new AvailablePowerUp());
	}
	
	
	@Override
	public Bitmap draw()
	{
		sprites.loopAnim();
		return (sprites.getSprite(sprites.currentFrame));
	}

	@Override
	public void HandleMessage(Telegram msg)
	{
		
	}

}

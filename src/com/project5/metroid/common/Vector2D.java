package com.project5.metroid.common;

import java.io.Serializable;

import android.util.Log;

public class Vector2D implements Serializable
{
	//Android logcat debug
	private static final String TAG = "Vector2D";
	
	/**
	 * X coordinate
	 */
	public float x;
	/**
	 * Y coordinate
	 */
	public float y;

	/**
	 * Vector2D constructor creates an object that has an x and y coordinate.
	 * 
	 * @param x
	 *            - float x coordinate
	 * @param y
	 *            - float y coordinate
	 */

	public Vector2D(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2D()
	{
		this.x = 0;
		this.y = 0;
	}

	public Vector2D(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2D(Vector2D position)
	{
		this.x = position.x;
		this.y = position.y;
	}

	/**
	 * Dot operation for 2D vector matrix.
	 * 
	 * @param v2
	 *            vector to be dotted.
	 * @return returns this vector dotted with v2
	 */

	public float dot(Vector2D v2)
	{
		return this.x * v2.x + this.y * v2.y;
	}

	/**
	 * finds the length of a 2D vector
	 * 
	 * @return length of the vector
	 */

	public float getLength()
	{
		return (float) Math.sqrt(x * x + y * y);
	}

	/**
	 * Calculates the distance between two 2D vectors
	 * 
	 * @param v2
	 *            second Vector2D
	 * @return distance between this vector and v2
	 */

	public float getDistance(Vector2D v2)
	{
		return (float) Math.sqrt((v2.x - x) * (v2.x - x) + (v2.y - y) * (v2.y - y));
	}

	/**
	 * Adds two vectors together.
	 * 
	 * @param v2
	 *            second Vector2D
	 * @return returns a vector equal to the sum of the other two vectors.
	 */

	public Vector2D add(Vector2D v2)
	{
		return new Vector2D((x + v2.x), (y + v2.y));
	}

	/**
	 * Subtracts two vectors.
	 * 
	 * @param v2
	 *            second Vector2D
	 * @return returns a vector equal to the difference of the other two
	 *         vectors.
	 */

	public Vector2D subtract(Vector2D v2)
	{
		return new Vector2D((this.x - v2.x), (this.y - v2.y));
	}

	/**
	 * Multiplies a vector by a scalar
	 * 
	 * @param scaleFactor
	 *            - float amount to scale vector by
	 * @return returns a vector equal to this vector scaled by scaleFactor
	 */

	public Vector2D multiply(float scaleFactor)
	{
		return new Vector2D((this.x * scaleFactor), (this.y * scaleFactor));
	}

	/**
	 * Normalizes a vector to be a unit vector
	 * 
	 * @return a new vector equal to the unit vector of this vector.
	 */

	public Vector2D normalize()
	{
		float len = getLength();

		if (len != 0.0f)
		{
			this.x = (this.x / len);
			this.y = (this.y / len);
		}

		else
		{
			this.x = (0.0f);
			this.y = (0.0f);
		}

		return this;
	}

	public int getY()
	{
		return (int) this.y;
	}

	public int getX()
	{
		return (int) this.x;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}
}

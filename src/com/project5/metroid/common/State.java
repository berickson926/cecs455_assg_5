package com.project5.metroid.common;

/*
* State template: All behavior states must extend class State for its respective entity
*/

public abstract class State<Entity>
{	
	public abstract void enter(Entity E);

	public abstract void execute(Entity E);

	public abstract void exit(Entity E);
	
	//This executes if the agent receives a message from the dispatcher
	public abstract boolean onMessage(Entity E, Telegram telegram);
}

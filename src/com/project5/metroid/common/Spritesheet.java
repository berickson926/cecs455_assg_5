package com.project5.metroid.common;

import android.graphics.Bitmap;
import android.util.Log;

//import java.awt.image.BufferedImage;
 
public class Spritesheet
{ 
	//For Android debug
	public static final String TAG = "SPRITESHEET";
	
        private final int width;
        private final int height;
        private final int rows;
        private final int columns;
        int			maxFrames;
        public int	currentFrame;
        int			currentCycle;
        int			startingFrame;
        int			endingFrame;
        int			animSpeed;
        public boolean		isLooping;
       
       // public BufferedImage[] Sprite;
        private Bitmap[] Sprite;
       
        
        public Spritesheet (Bitmap Sheet, int Width, int Height, int Rows, int Columns)
        {
        	
                this.width = Width;						//Width of sprite
                this.height = Height;					//height of sprite
                this.rows = Rows;
                this.columns = Columns;
                this.Sprite = new Bitmap[rows * columns];
            	this.animSpeed = 2;
            	this.isLooping = true;
            	this.currentFrame = 1;
            	this.currentCycle = 0;
            	this.startingFrame = 1;
            	this.maxFrames = rows * columns;
            	this.endingFrame = this.maxFrames;
            	splitSprites(Sheet);
              
        }
       
        private void splitSprites (Bitmap sheet)
        {
        	int id = 0;
            for (int i = 0; i < rows; i++)
            {
            	for (int j = 0; j < columns; j++)
                {
            		this.Sprite[id] = Bitmap.createBitmap(sheet, j*width, i*height, width, height);
            		id++;
                }
            }
        }
       
        public Bitmap getSprite (int spriteID)
        {
                return Sprite[spriteID];
        }
        
        public int getStartingFrame()
        {
        	return startingFrame;
        }
        
        public void loopAnim()
        {
        	if (currentCycle >= animSpeed)
        	{
        		currentCycle = 0;
        		if (currentFrame == endingFrame && isLooping)
        			currentFrame = startingFrame;
        		else if (isLooping)
        			currentFrame++;
        	}
        	else
        		currentCycle++;
        }

        boolean isLastFrame()
        {
        	if (currentFrame == endingFrame)
        		return true;
        	return false;
        }

        public void setAnimSpeed(int s)
        {
        	animSpeed = s;		
        }

        public void setPlayingFrames(int s, int e)
        {
        	startingFrame = s;
        	currentFrame = s;
        	endingFrame = e+s;
        	currentCycle = 0;
        	isLooping = true;
        }
        void setPlayingFramesNoLoop(int s, int e)
        {
        	startingFrame = s;
        	currentFrame = s;
        	endingFrame = e+s;
        	currentCycle = 0;
        	isLooping = false;
        }
        
        
}

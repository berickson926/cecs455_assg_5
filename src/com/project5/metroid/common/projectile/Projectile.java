package com.project5.metroid.common.projectile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.project5.metroid.R;
import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.Spritesheet;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;

public class Projectile extends BaseGameEntity
{
	public static final String TAG = "Projectile";
	
	Context context;
	Bitmap bitmap;
	Spritesheet sprites;
	int damage;
	int width;
	int height;
	
	public Projectile(Context context, Bitmap bitmap)
	{
		Log.d(TAG, "New projectile made");
		this.context = context;
		//bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.m1samus_fixed);
		this.bitmap = bitmap;
		
		//width = (int) (27+27); //size adjustment, need to figure out better way
		//height= (int) (40+41); //to compensate for resolution change
		//sprites = new Spritesheet(bitmap, 27, 40, 9, 12); //original
		//sprites = new Spritesheet(bitmap,width,height,9,12);//debug, adjusting for size :(
		
		//sprites.setPlayingFrames(6, 0);
		//sprites.setAnimSpeed(5);
		//Log.d(TAG,"Acquired bitmap");
		stateMachine.SetCurrentState(new MoveRight());
		stateMachine.changeState(new MoveRight());
		//Log.d(TAG,"Set stateMachine");
		
	}
	
	@Override
	public Bitmap draw()
	{
		//sprites.loopAnim();
		//return sprites.getSprite(sprites.currentFrame);
		//Log.d(TAG,"Drawing projectile");
		return bitmap;
	}

	@Override
	public void HandleMessage(Telegram msg)
	{
		
	}
	
	public boolean isColliding()
	{
		Log.d(TAG, "Entering proj. collision");
		LevelManager lm = LevelManager.getInstance();
		TileMap map = lm.getLevel();
		Log.d(TAG, "Proj. coord: "+TileMap.pixelsToTiles((int) this.position.x)+","+TileMap.pixelsToTiles((int) this.position.y));
		int tempX, tempY;
		
		tempX = TileMap.pixelsToTiles((int) this.position.x);
		tempY = TileMap.pixelsToTiles((int) this.position.y);
		
		if(map.getTile(tempX, tempY) != null)
		{
			Log.d(TAG,"Found  Collision!!");
			return true; 
		}
		else
		{
			Log.d(TAG,"No  collision");
			return false;
		}
	}

}

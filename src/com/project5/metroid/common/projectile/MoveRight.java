package com.project5.metroid.common.projectile;

import java.util.Iterator;

import android.util.Log;

import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.EntityManager;
import com.project5.metroid.common.MessageDispatcher;
import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;
import com.project5.metroid.common.Player.Player;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;

public class MoveRight extends State<Projectile>
{

	public static final String TAG = "proj.MoveRight";
	@Override
	public void enter(Projectile E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute(Projectile E)
	{
		EntityManager em = EntityManager.getInstance();
		LevelManager lm = LevelManager.getInstance();
		Boolean hit = false;
		
		if(E.isColliding())
		{
			//We've hit a wall tile. Delete ourselves
			lm.getLevel().removeGameObject(E); 
		}
		
		int pX = TileMap.pixelsToTiles((int) E.position.x);
		int pY = TileMap.pixelsToTiles((int) E.position.y);
		
		Iterator<BaseGameEntity> i = lm.getLevel().spriteList.iterator();
		int size = lm.getLevel().spriteList.size();
        while(i.hasNext() && lm.getLevel().spriteList.size() == size) 
        {
        	BaseGameEntity entity = (BaseGameEntity)i.next();
        	int x = Math.round(entity.position.x);
            int y = Math.round(entity.position.y);
            x = TileMap.pixelsToTiles(x);
            y = TileMap.pixelsToTiles(y);
            
            
            
        	if (x >= 0 && x < LevelManager.SCREEN_WIDTH)
            {
            	if(x == pX && y == pY && entity.getId() != E.getId())
            	{
            		Log.d(TAG,"PROJECTILE HIT!!!!");
            		Log.d(TAG,"Hit an entity! id:"+entity.getId());
            		Log.d(TAG,"Enemy is: "+em.getEntityFromID(entity.getId()).name);
            		//Log.d(TAG,"Player id: "+Player.playerID);
            		MessageDispatcher md = MessageDispatcher.getInstance();
        			Log.d(TAG,"Proj. sending message");
        			md.dispatch(0, E, em.getEntityFromID(entity.getId()), message_type.ATTACKED, E.damage);
        			Log.d(TAG,"Message sent");
        			hit = true;
        			lm.getLevel().removeGameObject(E);
            	}
            }
        }
        
        if(hit == false)
        {
        	//Log.d(TAG,"Projectile moving right");
        	E.position.x += 5;
        }
	}

	@Override
	public void exit(Projectile E)
	{
		
	}

	@Override
	public boolean onMessage(Projectile E, Telegram telegram)
	{
		// TODO Auto-generated method stub
		return false;
	}

}

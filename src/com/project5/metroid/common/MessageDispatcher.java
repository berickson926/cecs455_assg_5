package com.project5.metroid.common;

import java.util.Set;

import android.util.Log;

public class MessageDispatcher
{
	public static final String TAG = "MessageDispatcher";
	
	private static MessageDispatcher messageDispatcherInstance = null;
	
	/*
	 * Container for the delayed messages for auto-sort
	 * and avoidance of duplicates. messages sorted by dispatch
	 * time.
	 */
	private Set<Telegram> priorityQ;
	
	/*This method is utilized by dispatchMessage or
	 * dispatchDelayedMessages.
	 * 
	 * This method calls the message handling memeber function
	 * of the receiving entity, pReceiver, with the newly 
	 * created telegram
	 */
	private void discharge(BaseGameEntity receiver, Telegram msg)
	{
		receiver.HandleMessage(msg);
	}
	
	public MessageDispatcher()
	{
		
	}
	
	public void dispatch(    double 			delay,		//Delay until message sent, otherwise 0
							 BaseGameEntity 	sender,		//object sending
							 BaseGameEntity 	receiver,	//object receiving
							 message_type		msg,		//enum indicating what message it is
							 int				extraInfo)	//associated value with message 
	{														//(i.e. an ATTACK msg, includes the attack damage value)
		
		//Get a pointer to the receiver of the message
		//<E> pReceiver = EntityMgr->GetEntityFromID(receiver);
		
		//create the telegram
		Telegram telegram = new Telegram( sender, receiver, msg,0, extraInfo);
		
		//If there is no delay, route the telegram immediately
		if(delay <= 0.0)
		{
			//send the telegram to the recipient
			Log.d(TAG,"Discharging message to: "+receiver.id);
			discharge(receiver, telegram);
			
		}
		else//calculate the time when telegram should be dispatched
		{
			//Disabled for now, for our current purposes no messages are being delayed
			
			/*double currentTime = Clock.GetCurrentTime();
			
			telegram.dispatchTime = currentTime + delay;
			
			//and put it in the queue
			priorityQ.insert(telegram);*/
		}
	}//end constructor
	
	public static synchronized MessageDispatcher getInstance()
	{
		if (messageDispatcherInstance == null)
		{
			messageDispatcherInstance = new MessageDispatcher();
		}
		return messageDispatcherInstance;
	}
}

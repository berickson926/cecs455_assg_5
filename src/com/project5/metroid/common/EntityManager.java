package com.project5.metroid.common;

import java.util.Map;
import java.util.TreeMap;


public class EntityManager
{
	//This class is a singleton :(
	public static EntityManager instance = null;
	
	//To facilitate quick lookup the entities are stored in a std::map, in which pointers
	//to entities are cross-refernced by their identifying number
	private Map<Integer,BaseGameEntity> EntityMap;
	private int id;
		
	public enum EntityType
	{
		BASE, MONSTER, FOREGROUNDTILE, POWERUP, PLAYER, BUTTON;
	}

	public EntityManager()
	{
		EntityMap = new TreeMap<Integer,BaseGameEntity>();
		id = 1;//the starting ID available for assignment
	}

	
	public static synchronized EntityManager getInstance()
	{
		if (instance == null)
			instance = new EntityManager();
		
		return instance;
	}
	
	//This method stores a pointer to the entity in the std::vector m_Entities at the 
	//index position indicated by the entity's ID. Makes for faster access
	public int RegisterEntity(BaseGameEntity newEntity)
	{
		id++;
		EntityMap.put(id, newEntity);
		return id;//The id is returned so an object can know what its own ID is. For now.
	}
	
	//Returns a pointer to the entity with the ID given as a parameter
	public BaseGameEntity getEntityFromID(int id)
	{
		return EntityMap.get(id);
	}
	
	//this method removes the entity from the list
	public void removeEntity(BaseGameEntity entity)
	{
		//Not sure if we wish to actually remove entities for now. Intentionally blank
		//Possibly wish to overload to request entity removal via id instead
	}
}

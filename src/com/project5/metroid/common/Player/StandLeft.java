package com.project5.metroid.common.Player;

import android.util.Log;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;

public class StandLeft extends State<Player>
{
	//Android logcat debug
	public static final String TAG = "STANDLEFT";
	
	@Override
	public void enter(Player E)
	{
		Log.d(TAG,"Enter: startingframe: "+E.sprites.getStartingFrame());
		if(E.sprites.getStartingFrame() != 52) //Don't change sprite frame if we're in ball form
			E.sprites.setPlayingFrames(28, 0);
	}

	@Override
	public void execute(Player E)
	{
		if(!E.isColliding("DOWN"))
		{
			E.stateMachine.changeState(new FallLeft());
		}
	}

	@Override
	public void exit(Player E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		//Handle Gamepad input
		if(telegram.message == message_type.MOVELEFT || telegram.message == message_type.MOVELEFT_AIM_UP
		 ||telegram.message == message_type.MOVELEFT_ATTACK_UP || telegram.message == message_type.MOVELEFT_ATTACK)
		{
			Log.d(TAG,"Received move left message");
			E.stateMachine.changeState(new MoveLeft());
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT || telegram.message == message_type.MOVERIGHT_AIM_UP
		      ||telegram.message == message_type.MOVERIGHT_ATTACK_UP || telegram.message == message_type.MOVERIGHT_ATTACK)
		{
			E.stateMachine.changeState(new MoveRight());
			return true;
		}
		else if(telegram.message == message_type.AIM_UP)
		{
			//Adjust location when stepping out of ball form
			if(E.sprites.getStartingFrame() == 52)
				E.position.y -= E.spriteHeight;
	
			E.sprites.setPlayingFrames(40, 0);
			
			return true;
		}
		else if(telegram.message == message_type.NOACTION)
		{
			
			if(E.sprites.getStartingFrame() != 64 || E.sprites.getStartingFrame() != 16)
			{
				if(E.sprites.getStartingFrame() != 52)
					E.sprites.setPlayingFrames(28, 0);
			}
			return true;
		}
		else if(telegram.message == message_type.JUMP || telegram.message == message_type.JUMPLEFT)
		{
			if(E.sprites.getStartingFrame() != 52)
				E.stateMachine.changeState(new JumpLeft());
			return true;
		}
		else if(telegram.message == message_type.DUCK || telegram.message == message_type.DUCKLEFT || 
				telegram.message == message_type.DUCKRIGHT)
		{
			if(E.sphere == true && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(52, 3);
			return true;
		}
		return false;
	}

}

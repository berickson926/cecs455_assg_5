package com.project5.metroid.common.Player;

import android.util.Log;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;
import com.project5.metroid.common.projectile.Projectile;

public class StandRight extends State<Player>
{
	//Android logcat debug
	public static final String TAG = "STANDRIGHT";
	
	int fireDelay;
	
	@Override
	public void enter(Player E)
	{
		if(E.sprites.getStartingFrame() != 52) //Don't change sprite frame if we're in ball form
			E.sprites.setPlayingFrames(30, 0);
		
		fireDelay = 0;
	}

	@Override
	public void execute(Player E)
	{
		if(!E.isColliding("DOWN"))
		{
			E.stateMachine.changeState(new FallRight());
		}
	}

	@Override
	public void exit(Player E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		//Handle movement changes
		if(telegram.message == message_type.MOVERIGHT || telegram.message == message_type.MOVERIGHT_AIM_UP
		 ||telegram.message == message_type.MOVERIGHT_ATTACK || telegram.message == message_type.MOVERIGHT_ATTACK_UP)
		{
			Log.d(TAG,"Receieved MoveRight message from gamepad");
			E.stateMachine.changeState(new MoveRight());
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT || telegram.message == message_type.MOVELEFT_AIM_UP
			  ||telegram.message == message_type.MOVELEFT_ATTACK || telegram.message == message_type.MOVELEFT_ATTACK_UP)
		{
			E.stateMachine.changeState(new MoveLeft());
			return true;
		}
		
		//handle weapon aiming changes
		else if(telegram.message == message_type.AIM_UP)
		{
			//Adjust location when stepping out of ball form
			if(E.sprites.getStartingFrame() == 52)
				E.position.y -= E.spriteHeight;
			
			//Aiming up, facing right
			Log.d(TAG,"Aiming up right");
			E.sprites.setPlayingFrames(42, 0);
			return true;
		}
		else if(telegram.message == message_type.NOACTION)
		{
			if(E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(30, 0);
			return true;
		}
		else if(telegram.message == message_type.JUMP || telegram.message == message_type.JUMPRIGHT)
		{
			if(E.sprites.getStartingFrame() != 52)
				E.stateMachine.changeState(new JumpRight());
			return true;
		}
		else if(telegram.message == message_type.DUCK || telegram.message == message_type.DUCKLEFT || 
				telegram.message == message_type.DUCKRIGHT)
		{
			if(E.sphere == true && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(52, 3);
			return true;
		}
		else if(telegram.message == message_type.ATTACK)
		{
			
			Log.d(TAG, "Received attack command!");
			
			LevelManager lm = LevelManager.getInstance();
			
			if(fireDelay <= 0)
			{
				int pX = TileMap.pixelsToTiles((int) E.position.x);
				int pY = TileMap.pixelsToTiles((int) E.position.y);
				Log.d(TAG,"Firing from player coord: "+pX+","+pY);
				lm.getLevel().addGameObject(new Projectile(E.context, E.sprites.getSprite(6)), 
						pX+2, pY+2);
				Log.d(TAG,"Projectile added from player");
				fireDelay = 10;
			}
			else
			{
				Log.d(TAG,"Weapong cooling");
				fireDelay--;
			}
		}
		return false;
	}

}

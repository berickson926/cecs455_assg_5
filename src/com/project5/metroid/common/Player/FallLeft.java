package com.project5.metroid.common.Player;

import android.util.Log;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;

public class FallLeft extends State<Player>
{
	//Android logcat debug
	private static final String TAG = "FallLeft";
	
	int gravity;
	
	@Override
	public void enter(Player E)
	{
		if(E.sprites.getStartingFrame() != 52)
			E.sprites.setPlayingFrames(12, 0);
		
	}

	@Override
	public void execute(Player E)
	{
		//If we are not touching ground
		if(!E.isColliding("DOWN"))
		{
			E.position.setY(E.position.getY() + E.gravity/2);
			if (E.gravity < 20)
				E.gravity++;
		}
		//Stop falling if we've hit ground
		else
		{
			Log.d(TAG,"Changing state into standleft");
			E.gravity = 2;
			E.stateMachine.changeState(new StandLeft());
		}	
		
	}

	@Override
	public void exit(Player E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		if(telegram.message == message_type.MOVELEFT || telegram.message == message_type.JUMPLEFT)
		{
			if(!E.isColliding("LEFT"))
				E.position.setX(E.getPosition().getX() - E.getVelocity());
			
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT_ATTACK)
		{
			if(E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(24, 0);
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT_ATTACK_UP || telegram.message == message_type.AIM_UP)
		{
			//Adjust location when stepping out of ball form
			if(E.sprites.getStartingFrame() == 52)
				E.position.y -= E.spriteHeight;
			
			E.sprites.setPlayingFrames(36, 0);
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT || telegram.message == message_type.MOVERIGHT_AIM_UP
			 || telegram.message == message_type.MOVERIGHT_ATTACK || telegram.message == message_type.MOVERIGHT_ATTACK_UP
			 || telegram.message == message_type.JUMPRIGHT)
		{
			E.stateMachine.changeState(new FallRight());
			return true;
		}
		else if(telegram.message == message_type.DUCK || telegram.message == message_type.DUCKLEFT || 
				telegram.message == message_type.DUCKRIGHT)
		{
			if(E.sphere == true && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(52, 3);
			return true;
		}
		return false;
	}
}

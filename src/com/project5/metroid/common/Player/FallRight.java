package com.project5.metroid.common.Player;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;

public class FallRight extends State<Player>
{
	public static final String TAG = "FallRight";
	@Override
	public void enter(Player E)
	{
		if(E.sprites.getStartingFrame() != 52)
			E.sprites.setPlayingFrames(21, 0);
	}

	@Override
	public void execute(Player E)
	{
		//If we are not touching ground
		if(!E.isColliding("DOWN"))
		{
			E.position.setY(E.position.getY() + E.gravity/2);
			if (E.gravity < 20)
				E.gravity++;
		}
		//Stop falling if we've hit ground
		else
		{
			E.gravity = 2;
			E.stateMachine.changeState(new StandRight());
		}
	}

	@Override
	public void exit(Player E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		if(telegram.message == message_type.MOVERIGHT || telegram.message == message_type.JUMPRIGHT)
		{
			if(!E.isColliding("RIGHT"))
				E.position.setX(E.getPosition().getX() + E.getVelocity());
			
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT_ATTACK)
		{
			if(E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(34, 0);
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT_ATTACK_UP || telegram.message == message_type.AIM_UP)
		{
			//Adjust location when stepping out of ball form
			if(E.sprites.getStartingFrame() == 52)
				E.position.y -= E.spriteHeight;
			
			E.sprites.setPlayingFrames(46, 0);
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT || telegram.message == message_type.MOVELEFT_AIM_UP ||
				telegram.message == message_type.MOVELEFT_ATTACK || telegram.message == message_type.MOVELEFT_ATTACK_UP ||
				telegram.message == message_type.JUMPLEFT)
		{
			E.stateMachine.changeState(new FallLeft());
			return true;
		}
		return false;
	}
}

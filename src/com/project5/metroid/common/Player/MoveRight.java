package com.project5.metroid.common.Player;

import android.util.Log;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;

public class MoveRight extends State<Player>
{
	//Android logcat debug
	public static final String TAG = "MOVERIGHT";
	
	@Override
	public void enter(Player E)
	{
		if(!E.isColliding("RIGHT"))
		{
			if(!(E.stateMachine.getPreviousState() instanceof JumpRight) || !(E.stateMachine.getPreviousState() instanceof JumpLeft))
			{
				if(E.sprites.getStartingFrame() != 52)//don't change if we're in ball form
					E.sprites.setPlayingFrames(18, 2);
			}
		}
		else
			E.stateMachine.changeState(new StandRight());
	}

	@Override
	public void execute(Player E)
	{
		if(E.isColliding("DOWN") && !E.isColliding("RIGHT"))
		{
			E.position.setX(E.getPosition().getX() + E.getVelocity());
			if(E.stateMachine.getPreviousState() instanceof FallRight)
				E.stateMachine.changeState(new FallRight());
		}
		else
			E.stateMachine.changeState(new FallRight());
	}

	@Override
	public void exit(Player E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		if(telegram.message == message_type.MOVELEFT || telegram.message == message_type.MOVELEFT_AIM_UP)
		{
			Log.d(TAG,"received move left message;moving into move left");
			E.stateMachine.changeState(new MoveLeft());
			return true;
		}
		else if(telegram.message == message_type.NOACTION || telegram.message == message_type.AIM_UP)
		{
			Log.d(TAG,"Received noaction message;moving into standright");
			E.stateMachine.changeState(new StandRight());
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT_AIM_UP || telegram.message == message_type.MOVERIGHT_ATTACK_UP)
		{
			//Adjust location when stepping out of ball form
			if(E.sprites.getStartingFrame() == 52)
				E.position.y -= E.spriteHeight;
			
			if(E.sprites.getStartingFrame() != 43)
				E.sprites.setPlayingFrames(43, 2);
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT)
		{
			if(E.sprites.getStartingFrame() != 18 && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(18, 2);
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT_ATTACK)
		{
			if(E.sprites.getStartingFrame() != 31 && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(31, 2);
			return true;
		}
		else if(telegram.message == message_type.JUMP || telegram.message == message_type.JUMPRIGHT)
		{
			if(E.sprites.getStartingFrame() != 52)//don't jump in ball form
				E.stateMachine.changeState(new JumpRight());
			return true;
		}
		return false;
	}

}

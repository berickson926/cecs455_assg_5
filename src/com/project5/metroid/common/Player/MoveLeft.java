package com.project5.metroid.common.Player;

import android.util.Log;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;

public class MoveLeft extends State<Player>
{
	//Android logcat debug
	public static final String TAG = "MOVELEFT";
	
	@Override
	public void enter(Player E)
	{
		if(!E.isColliding("LEFT"))
		{
			if(!(E.stateMachine.getPreviousState() instanceof JumpRight) || !(E.stateMachine.getPreviousState() instanceof JumpLeft))
			{
				if(E.sprites.getStartingFrame() != 52)//don't change if we're in ball form
					E.sprites.setPlayingFrames(13, 2);
			}
		}
		else
			E.stateMachine.changeState(new StandLeft());
	}

	@Override
	public void execute(Player E)
	{
		if(E.isColliding("DOWN") && !E.isColliding("LEFT"))
		{
			E.position.setX(E.getPosition().getX() - E.getVelocity());
			if(E.stateMachine.getPreviousState() instanceof FallLeft)
				E.stateMachine.changeState(new FallLeft());
			else if(E.stateMachine.getPreviousState() instanceof JumpLeft)
				E.stateMachine.changeState(new JumpLeft());															
		}
		else
			E.stateMachine.changeState(new FallLeft());
	}

	@Override
	public void exit(Player E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		if(telegram.message == message_type.MOVERIGHT || telegram.message == message_type.MOVERIGHT_AIM_UP)
		{
			Log.d(TAG,"Received moveright request, changing to moveright state");
			E.stateMachine.changeState(new MoveRight());
			return true;
		}
		else if(telegram.message == message_type.NOACTION || telegram.message == message_type.AIM_UP)
		{
			Log.d(TAG,"Received noaction; moving to standleft state");
			E.stateMachine.changeState(new StandLeft());
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT_AIM_UP || telegram.message == message_type.MOVELEFT_ATTACK_UP)
		{
			//Adjust location when stepping out of ball form
			if(E.sprites.getStartingFrame() == 52)
				E.position.y -= E.spriteHeight;
			
			if(E.sprites.getStartingFrame() != 37)
				E.sprites.setPlayingFrames(37, 2);
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT)
		{
			if(E.sprites.getStartingFrame() != 13 && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(13, 2);
			return true;
		}
		else if(telegram.message == message_type.MOVELEFT_ATTACK)
		{
			//TODO: create projectile
			if(E.sprites.getStartingFrame() != 25 && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(25, 2);
			return true;
		}
		else if(telegram.message == message_type.JUMP || telegram.message == message_type.JUMPLEFT)
		{
			if(E.sprites.getStartingFrame() != 52) //Don't jump in ball form
				E.stateMachine.changeState(new JumpLeft());
			return true;
		}
		return false;
	}

}

package com.project5.metroid.common.Player;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;

public class JumpRight extends State<Player>
{
	
	
	@Override
	public void enter(Player E)
	{
		E.sprites.setPlayingFrames(21, 0);
		E.gravity = 2;
	}

	@Override
	public void execute(Player E)
	{
		if(!E.isColliding("UP"))
		{
			E.position.y = E.position.y - 8;
		}
		if (E.jumpCycle == 0)
		{
			E.stateMachine.changeState(new FallRight());
			E.jumpCycle=23;
		}
		E.jumpCycle--;
	}

	@Override
	public void exit(Player E)
	{
		
	}

	@Override
	public boolean onMessage(Player E, Telegram telegram)
	{
		if(telegram.message == message_type.MOVERIGHT_ATTACK)
		{
			E.sprites.setPlayingFrames(34, 0);
			return true;
		}
		else if(telegram.message == message_type.NOACTION)
		{
			E.sprites.setPlayingFrames(21, 0);
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT || telegram.message == message_type.JUMPRIGHT)
		{
			E.sprites.setPlayingFrames(21, 0);
			if(!E.isColliding("RIGHT"))
				E.position.setX(E.getPosition().getX() + E.getVelocity());
			return true;
		}
		else if(telegram.message == message_type.MOVERIGHT_AIM_UP || telegram.message == message_type.MOVERIGHT_ATTACK_UP
				|| telegram.message == message_type.AIM_UP)
		{
			E.sprites.setPlayingFrames(46, 0);
			return true;
		}
		else if(telegram.message == message_type.DUCK || telegram.message == message_type.DUCKLEFT || telegram.message == message_type.DUCKRIGHT)
		{
			if(E.sphere == true && E.sprites.getStartingFrame() != 52)
				E.sprites.setPlayingFrames(52, 3);
			E.stateMachine.changeState(new FallRight());
			return true;
		}
		
		return false;
	}

}

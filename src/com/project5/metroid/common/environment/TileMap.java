package com.project5.metroid.common.environment;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;

import com.project5.metroid.R;
import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.EntityManager;
import com.project5.metroid.common.Spritesheet;
import com.project5.metroid.common.Vector2D;
import com.project5.metroid.common.Player.Player;
import com.project5.metroid.common.monster.Crawler;
import com.project5.metroid.common.powerup.BallUpgrade;
import com.project5.metroid.common.projectile.Projectile;

public class TileMap
{
	//Android logcat debugging
	private static final String TAG = "TileMap";
	
	public static int TILE_SIZE = 32;//One tile == 32 pixels in length/width
	
	BaseGameEntity map[][];
	public ArrayList<BaseGameEntity> spriteList;
	

	private String 		name;
	public int			width;
	private int			mapPixelWidth;
	private int			height;
	private int			mapPixelHeight;
	public int			offsetX;
	public int			offsetY;
	private int			firstTileX;
	private int			firstTileY;
	private int			lastTileX;
	private int			lastTileY;
	
	
	private Context 	context;
	private Bitmap 		bitmap;
	private Spritesheet	sprites;
	
	private int 		viewScreenX;
	private int 		viewScreenY;
	
	
	public TileMap(Context context)
	{
		Log.d(TAG,"Creating a new TileMap");
		name = "Level1";
		
		//Set up screen access via context 
		this.context = context;
		
		//set up access to the level tile sprite sheet
		bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.m1masterset);
		sprites = new Spritesheet(bitmap,32,32,35,15);
		
		width = 100;
		height = 15;
		map = new BaseGameEntity[width][height];
		spriteList = new ArrayList<BaseGameEntity>();
		
		//DEBUG:Hardcoding nodes for testing
		//TODO:Migrate initialization into levelManager & XML
		testMapSetup();
		
		//starting location viewing screen
		EntityManager em = EntityManager.getInstance();
		Vector2D pLoc = em.getEntityFromID(Player.playerID).getPosition(); //get location of the player
		offsetX = (int) LevelManager.SCREEN_WIDTH / 2 - pLoc.getX() - TILE_SIZE;
		
		mapPixelWidth = tilesToPixels(width);
		mapPixelHeight = tilesToPixels(height);
		
		offsetY = (int) (LevelManager.SCREEN_HEIGHT - tilesToPixels(height));
		
	}//end constructor

	//Used for pinning static objects, such as world map tiles to the game grid
	private void addGameTile(GameTile tile, int tileX, int tileY)
	{
		if (tile != null) 
		{	
			/*
			Vector2D tempLoc = new Vector2D();
			
			//center the tile
			int newX = (int) (tilesToPixels(tileX)+tilesToPixels(1) - tile.getWidth()/2);
			tempLoc.setX(newX);
			
			//bottom-justify the tile
			tempLoc.setY((int) ((int) tilesToPixels(tileY + 1) - tile.getHeight()));
			
			
			//add it to the map & update the tile with this info
			tile.location = tempLoc;*/
			tile.location.setX(tileX);
			tile.location.setY(tileY);
			map[tileX][tileY] = tile;
		}
	}
	
	//Used for pinning non-static objects(player, monsters, etc.) to the game grid
	public void addGameObject(BaseGameEntity entity, int tileX, int tileY)
	{
		Log.d(TAG,"Entered add gameObject");
		Log.d(TAG,"Obj. Tile coords: "+tileX+","+tileY);
		
		if(entity != null)
		{
			Vector2D tempLoc = new Vector2D();
			
			//Center the object
			int newX = (int) (tilesToPixels(tileX)+tilesToPixels(1) - entity.draw().getWidth()+9);
			tempLoc.setX(newX);
			
			//bottom-justify
			tempLoc.setY((int) ((int) tilesToPixels(tileY + 1) - entity.draw().getHeight()));
			
			//add it to the map & update the tile with this info, which is pixel coords, not tile coords
			//We want sprites to be able to move around freely by pixel coords, rather than in large tile
			//increments
			entity.position = tempLoc;
			
			
			if(!(entity instanceof Player))
			{
				Log.d(TAG,"Entity added to list!");
				spriteList.add(entity);
			}
		}
	}//end addGameObject
	
	public void removeGameObject(BaseGameEntity entity)
	{
		//May not work as planned; implement hashmap w/entity id as key?
		spriteList.remove(entity);
	}
	
	//converts map tile location to pixel location
	public static int tilesToPixels(int numTiles)
	{
		return numTiles * TILE_SIZE;
	}
	
	public static int pixelsToTiles(int numPixels)
	{
		//return  (int)Math.floor((float)numPixels / TILE_SIZE);
		return numPixels / TILE_SIZE;
    }
	
	public BaseGameEntity getTile(int x, int y) 
	{
        if (x < 0 || x >= width || y < 0 || y >= height)
        {
            return null;
        }
        else 
        {
            return map[x][y];
        }
    }
	
	//Draws the visible part of the map only
	public Canvas draw(Canvas canvas)
	{
		//Draw the tiles
		firstTileX = pixelsToTiles(-offsetX);
		lastTileX = firstTileX +pixelsToTiles((int) LevelManager.SCREEN_WIDTH) + 1;
		for (int y=0; y<height; y++) 
		{
			for (int x=firstTileX; x <= lastTileX; x++) 
			{
				if(getTile(x,y) != null)
				{
					Bitmap bitmap = getTile(x,y).draw();
					if (bitmap != null) 
					{
						
						canvas.drawBitmap(bitmap,tilesToPixels(x) + offsetX,tilesToPixels(y) + offsetY,null);
					
					}
				}
				
				//DEBUG:Draw the grid
				/*
				Paint paint = new Paint();
				paint.setColor(Color.GREEN);
				paint.setStyle(Paint.Style.STROKE);
				Path path = new Path();
				
				path.addRect(tilesToPixels(x)+offsetX,tilesToPixels(y)+offsetY,
						tilesToPixels(x)+offsetX+32, tilesToPixels(y)+offsetY+32, Direction.CW);
				
				canvas.drawPath(path, paint);
				paint.setColor(Color.WHITE);
				canvas.drawText(x+","+y, tilesToPixels(x)+offsetX, tilesToPixels(y)+offsetY, paint);
				*/
			}
		}
		
		//draw the player
		EntityManager em = EntityManager.getInstance();
	
		canvas.drawBitmap(em.getEntityFromID(Player.playerID).draw(),
	            Math.round(em.getEntityFromID(Player.playerID).getPosition().x) + offsetX,
	            Math.round(em.getEntityFromID(Player.playerID).getPosition().y) + offsetY,
	            null);

		//draw other things, like monsters/power ups
        Iterator<BaseGameEntity> i = spriteList.iterator();
        while(i.hasNext()) 
        {
            BaseGameEntity entity = (BaseGameEntity)i.next();
            int x = Math.round(entity.position.x) + offsetX;
            int y = Math.round(entity.position.y) + offsetY;
            canvas.drawBitmap(entity.draw(), x, y, null);
        }
		return canvas;
	}//end draw
	
	public void update()
	{
		EntityManager em = EntityManager.getInstance();
		
		//update all sprites on this level within viewing range
		em.getEntityFromID(Player.playerID).update();
		Iterator<BaseGameEntity> i = spriteList.iterator();
		int size = spriteList.size();
        while(i.hasNext() && spriteList.size() == size) 
        {
        	BaseGameEntity entity = (BaseGameEntity)i.next();
        	int x = Math.round(entity.position.x) + offsetX;
            int y = Math.round(entity.position.y) + offsetY;
        	if (x >= 0 && x < LevelManager.SCREEN_WIDTH)
            {
            	entity.update();
            }
        }
        
		//get the location of the player and move screen
		Vector2D pLoc = em.getEntityFromID(Player.playerID).getPosition(); //get location of the player
		
		offsetX = (int) LevelManager.SCREEN_WIDTH / 2 - pLoc.getX() - TILE_SIZE;
		//if(offsetX > 0)
		//	offsetX = 0;
		//offsetX = Math.min(offsetX, 0);
        //offsetX = (int) Math.max(offsetX, LevelManager.SCREEN_WIDTH - width);

        // get the y offset to draw all sprites and tiles
        offsetY = (int) (LevelManager.SCREEN_HEIGHT -tilesToPixels(height));
        
	}//end update
	
	//DEBUG: used for testing map & screen setup, should be replaced by XML level loader in levelManager.java
	private void testMapSetup()
	{
		//Add non-tile objects, monsters/powerups, etc.
		//BaseGameEntity ballUpgrade = new BallUpgrade(this.context);
		addGameObject(new BallUpgrade(this.context), 7,10);
		
		//add crawlers
		//BaseGameEntity crawler = new Crawler(this.context);
		addGameObject(new Crawler(this.context), 22,3);
		addGameObject(new Crawler(this.context), 28,3);
		addGameObject(new Crawler(this.context), 5 ,12);
		
		//Add level tiles
		GameTile tile = new GameTile(sprites.getSprite(30));
		GameTile tileb= new GameTile(sprites.getSprite(8));
		//set blocks defining edges of game grid
		//top & bottom
		for(int i=0; i< width; i++)
		{
			addGameTile(tileb,i,0);
			addGameTile(tileb,i,height-1);
		}
		//left/right
		for(int i=0; i< height; i++)
		{
			addGameTile(tileb,0,i);
			addGameTile(tileb,width-1,i);
		}
		
		//Starting platform
		for(int i=22; i<29; i++)
			addGameTile(tile,i,13);
		addGameTile(tileb, 22,10);
		addGameTile(tileb, 23,10);
		addGameTile(tileb, 27,10);
		addGameTile(tileb, 28,10);
		addGameTile(tileb, 22,8);
		addGameTile(tileb, 23,7);
		addGameTile(tileb, 27,8);
		addGameTile(tileb, 28,7);
		addGameTile(new GameTile(sprites.getSprite(22)), 22,9);
		addGameTile(new GameTile(sprites.getSprite(22)), 23,9);
		addGameTile(new GameTile(sprites.getSprite(22)), 23,8);
		addGameTile(new GameTile(sprites.getSprite(22)), 27,9);
		addGameTile(new GameTile(sprites.getSprite(22)), 28,9);
		addGameTile(new GameTile(sprites.getSprite(22)), 28,8);
		
		addGameTile(new GameTile(sprites.getSprite(27)), 22,7);
		addGameTile(new GameTile(sprites.getSprite(27)), 27,7);
		
		addGameTile(new GameTile(sprites.getSprite(54)), 22, 6);
		addGameTile(new GameTile(sprites.getSprite(54)), 23, 6);
		addGameTile(new GameTile(sprites.getSprite(54)), 27, 6);
		addGameTile(new GameTile(sprites.getSprite(54)), 28, 6);
		
		addGameTile(new GameTile(sprites.getSprite(29)),22, 5);
		addGameTile(new GameTile(sprites.getSprite(29)), 28,5);
		
		addGameTile(new GameTile(sprites.getSprite(12)),22,4);
		addGameTile(new GameTile(sprites.getSprite(12)),28,4);
		
		//Pillars for the power-up
		addGameTile(tile, 4,13);
		addGameTile(tile, 5,13);
		addGameTile(tile, 9,13);
		addGameTile(tile,10,13);
		addGameTile(tile, 7,12);
		addGameTile(tile, 7,11);
		addGameTile(new GameTile(sprites.getSprite(12)), 4,12);
		addGameTile(new GameTile(sprites.getSprite(12)), 10,12);
		addGameTile(new GameTile(sprites.getSprite(22)), 7,13);
		
		//Jumping obstacle 
		//upper square
		for(int i=11; i<15; i++)
		{
			addGameTile(new GameTile(sprites.getSprite(8)), i,5);
			addGameTile(new GameTile(sprites.getSprite(8)), i,8);
		}
		for(int i=5; i<8  ;i++)
		{
			addGameTile(new GameTile(sprites.getSprite(8)), 11,i);
			addGameTile(new GameTile(sprites.getSprite(8)), 14,i);
		}
		//middle square
		for(int i=12; i<16 ;i++)
		{
			addGameTile(new GameTile(sprites.getSprite(8)), i,7);
			addGameTile(new GameTile(sprites.getSprite(8)), i,10);
		}
		for(int i=7; i<10 ;i++)
		{
			addGameTile(new GameTile(sprites.getSprite(8)), 12,i);
			addGameTile(new GameTile(sprites.getSprite(8)), 15,i);
		}
		
		//lowest square
		for(int i=13; i<17  ;i++)
		{
			addGameTile(new GameTile(sprites.getSprite(8)), i,9);
			addGameTile(new GameTile(sprites.getSprite(8)), i,12);
		}
		for(int i=9; i<12 ;i++)
		{
			addGameTile(new GameTile(sprites.getSprite(8)), 13,i);
			addGameTile(new GameTile(sprites.getSprite(8)), 16,i);
		}
	}
}

package com.project5.metroid.common.environment;

import android.graphics.Bitmap;
import android.util.Log;

import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.Vector2D;

public class GameTile extends BaseGameEntity
{
	//android logcat debug
	private static final String TAG = "GameTile";
	
	Bitmap 		bitmap;
	Vector2D	location;
	
	public GameTile(Bitmap bitmap)
	{
		this.bitmap = bitmap;
		location = new Vector2D();
		location.setX(0);
		location.setY(0);
	}
	@Override
	public void HandleMessage(Telegram msg)
	{
		
	}
	
	public Bitmap draw()
	{
		return bitmap;
	}
	
	public Vector2D getLocation()
	{
		return location;
	}
	
	public void setLocation(Vector2D location)
	{
		this.location = location;
	}
	
	public float getWidth()
	{
		return bitmap.getWidth();
	}
	
	public float getHeight()
	{
		return bitmap.getHeight();
	}

}

package com.project5.metroid.common.environment;

import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;

import android.content.res.XmlResourceParser;
import android.graphics.Rect;
import android.util.Log;

public class LevelManager
{
	//Android logcat debug
	private static final String TAG = "LevelManager";
	
	//This class is a singleton :(
	private static LevelManager instance = null;
	
	public static float SCREEN_WIDTH;
	public static float SCREEN_HEIGHT;
	
	public ArrayList<TileMap>  levelList;
	private int					currentLevel;
	
	
	public LevelManager(Rect canvasInfo)
	{
		levelList = new ArrayList<TileMap>();
		currentLevel = 0;
		
		//						800					0			480					0
		Log.d(TAG,"canvasInfo: "+canvasInfo.right+canvasInfo.left+canvasInfo.bottom+canvasInfo.top);
		Log.d(TAG,"canvasInfo.width(): "+canvasInfo.width());
		SCREEN_WIDTH = canvasInfo.width();
		Log.d(TAG,"set SCREEN_WIDTH to: "+SCREEN_WIDTH);
		SCREEN_HEIGHT= canvasInfo.height();
		
	}
	
	public LevelManager()
	{
		levelList = new ArrayList<TileMap>();
		
	}
	
	public void addLevel(TileMap map)
	{
		levelList.add(map);
	}
	
	/**
	 * 
	 * @return returns the current active level
	 */
	public TileMap getLevel()
	{
		//Consider using a map to retrieve levels by name instead of an internal variable?
		return levelList.get(currentLevel);
	}

	public static synchronized LevelManager getInstance()
	{
		if (instance == null)
			instance = new LevelManager();
		
		return instance;
	}
	
	public void loadLevels(XmlResourceParser file)
	{
		
		int eventType; //Flag for node types
		
		try
		{
			file.next();//get next parse event
			eventType = file.getEventType();//get current xml event i.e., START_DOCUMENT, etc.
			
			String NodeValue;
			while(eventType != XmlPullParser.END_DOCUMENT)
			{
				if(eventType == XmlPullParser.START_DOCUMENT)
				{
					//Start of XML, can check this with myxml.getname() in log, see if your
					//xml has read successfully
					Log.d(TAG,"Started XML Read");
				}
				else if(eventType == XmlPullParser.START_TAG)
				{
					//the name of a start tag is available from file.getName()
					//its namespace and prefix is available from getNamespace()/getprefix
					//getAttribute for attributes
					
					Log.d(TAG,"Started:"+file.getName());
				}
				else if(eventType == XmlPullParser.END_TAG)
				{
					//End of document
					Log.d(TAG,"Reached end of XML: "+file.getName());
				}
				else if(eventType == XmlPullParser.TEXT)
				{
					//character data avail by calling file.getText();
				}
				
				eventType = file.next();
				
			}//end while
			
		}//end try
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} //end catch
	}//End load levels method
}//end level manager class

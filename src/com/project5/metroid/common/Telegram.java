package com.project5.metroid.common;

public class Telegram
{
	//the entity that sent this telegram
	BaseGameEntity sender;
	
	//the entity that is to receive this telegram
	BaseGameEntity receiver;
	
	//the message itself
	public message_type message;
	
	//messages can be dispatched immediately or delayed for
	//a specified amount of time. If a delay is necessar, this
	//field is stamped with the time the message should be dispatched
	double dispatchTime;
	
	//any additional information that may accompany the message
	public int extraInfo;
	
	public Telegram()
	{
		sender = null;
		receiver = null;
		message = null;
		dispatchTime = 0;
	}//end default constructor
	
	Telegram(BaseGameEntity sender, BaseGameEntity receiver, message_type message, double dispatchTime, int extraInfo)
	{
		this.sender = sender;
		this.receiver = receiver;
		this.message = message;
		this.dispatchTime = dispatchTime;
		this.extraInfo = extraInfo;
	}//end overloaded Constructor
}

package com.project5.metroid.common;

import android.graphics.Bitmap;
import android.util.Log;

import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;


/*
 *	Game objects such as monsters must extend BaseGameEntity to employ the behavior state machine system.
 *	Have each entity call super(); to start up the state machine
 *  use stateMachine.setCurrentState(new StateType()); to define the behaviors thereafter.
 */

public abstract class BaseGameEntity 
{
	//Android logcat debug
	public static final String TAG = "BaseGameEntity";
	
	public StateMachine<?> stateMachine; //set to public for debugging only! revert back

	protected int id;
	public Vector2D position;
	
	public int spriteWidth;
	public int spriteHeight;
	
	public String		name;
	
	public BaseGameEntity()
	{
		//Create the state machine for AI associated with this entity
		stateMachine = new StateMachine<BaseGameEntity>(BaseGameEntity.this);
		
		//Add this entity to the entity manager & get its unique ID
		EntityManager entityManager = EntityManager.getInstance();
		id = entityManager.RegisterEntity(this);
		
	}// end constructor
	
	public int getId()
	{
		return this.id;
	}
	
	public Vector2D getPosition()
	{
		return position;
	}

	public void update()
	{
		stateMachine.update();
	}
	
	//All base game entities must define their own draw method
	public abstract Bitmap draw();
	
	//Must be uniquely implemented for each inheriting entity
	public abstract void HandleMessage(Telegram msg);
	
	public boolean isColliding(String direction)
	{
		LevelManager lm = LevelManager.getInstance();
		TileMap map = lm.getLevel();
		
		//Check bounds of sprite box to see if we've collided with another tile
		int tempX,tempY;
		int tempXL,tempXR;
		if(direction == "DOWN")
		{
			tempX = TileMap.pixelsToTiles((int) this.position.x );
			tempXL= TileMap.pixelsToTiles((int) this.position.x - this.spriteWidth/2 - 7);
			tempXR= TileMap.pixelsToTiles((int) this.position.x + this.spriteWidth/2 - 7);
			tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight);
			
			//Log.d(TAG,"Checking for collision at: "+tempX+","+tempY);
			if(map.getTile(tempX+1, tempY) != null)
			{
				//Log.d(TAG,"Found "+direction+" Collision!!");
				
				return true;
			}
			else if(map.getTile(tempXL+1, tempY) != null)
			{
				//Log.d(TAG,"Found left edge down collision!");
				return true;
			}
			else if(map.getTile(tempXR+1, tempY) != null)
			{
				//Log.d(TAG, "Found right edge down collision!");
				return true;
			}
			else
				return false;
			
		}
		else if(direction == "UP")
		{
			//Log.d(TAG,"Testing up collision");
			tempX = TileMap.pixelsToTiles((int) this.position.x + (this.spriteWidth/2));
			tempY = TileMap.pixelsToTiles((int) this.position.y);
		}
		else if(direction == "LEFT")
		{	
			//Log.d(TAG,"Checking first Left collision");
			tempX = TileMap.pixelsToTiles((int) this.position.x - this.spriteWidth/2);
			tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight/2);
			//Log.d(TAG,"Checking for collision at: "+tempX+","+tempY);
			if(map.getTile(tempX+1, tempY) != null)
			{
				//Log.d(TAG,"Found "+direction+" Collision!!");
				return true;
			}
			
			tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight -10);
			//Log.d(TAG,"Checking (lower) for collision at: "+tempX+","+tempY);
			if(map.getTile(tempX+1, tempY) != null)
			{
				//Log.d(TAG,"Found "+direction+" Collision!!");
				return true;
			}
			else
				return false;
		}
		else if(direction == "RIGHT")
		{
			//Log.d(TAG,"Checking first right collision");
			tempX = TileMap.pixelsToTiles((int) this.position.x + this.spriteWidth/2);
			tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight/2);
			
			//Log.d(TAG,"Checking for collision at: "+tempX+","+tempY);
			if(map.getTile(tempX+1, tempY) != null)
			{
				//Log.d(TAG,"Found "+direction+" Collision!!");
				return true;
			}
			
			tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight -10);
			//Log.d(TAG,"Checking (lower) for collision at: "+tempX+","+tempY);
			if(map.getTile(tempX+1, tempY) != null)
			{
				//Log.d(TAG,"Found "+direction+" Collision!!");
				return true;
			}
			else
				return false;
		}
		else
		{
			tempX = TileMap.pixelsToTiles((int) this.position.x );
			tempY = TileMap.pixelsToTiles((int) this.position.y );
		}
		//Log.d(TAG,"Checking for collision at: "+tempX+","+tempY);
		if(map.getTile(tempX, tempY) != null)
		{
			//Log.d(TAG,"Found "+direction+" Collision!!");
			return true;
		}
		else
			return false;
		
		//Will need additional check for sprites?
	}
	
}// end abstract class BaseGameEntity

package com.project5.metroid.common.monster;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.project5.metroid.R;
import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.Spritesheet;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;

public class Crawler extends BaseGameEntity
{
	public static final String TAG = "Crawler";
	//Orientation is the direction the top of the crawler is pointing
	//i.e., if crawling along flat ground, orientationw will be UP
	//if crawling up a wall, orientation will be LEFT
	//crawling down a wall, orientation will be RIGHT, etc.
	public static enum orientation
	{
		UP,LEFT,RIGHT,DOWN
	}

	public 	orientation	orient;
	public  orientation prevOri;
	private Context		context;
	private Bitmap		bitmap;
	public Spritesheet sprites;
	public	int			hp;			//the amount of hp this monster has
	public	int			damage; 	//the amount of damage this entity deals to the player
	public 	int			spriteHeight;
	public 	int			spriteWidth;
	public	int			velocity;
	public Boolean		hitPlayer;
	
	
	public Crawler(Context context)
	{
		name = "Crawler";
		this.context = context;
		hp = 15;
		damage = 3;
		velocity = 1;
		hitPlayer = false;
		
		bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy_crawler);
		sprites = new Spritesheet(bitmap,(17+17),(14+14),1,15);
		sprites.setPlayingFrames(0, 1);
		sprites.setAnimSpeed(4);
		spriteWidth = sprites.getSprite(0).getWidth();
		spriteHeight= sprites.getSprite(0).getHeight();
		
		this.stateMachine.SetGlobalState(new CrawlerPatrol());
		this.stateMachine.SetCurrentState(new Move());
		
		orient = orientation.UP;
		prevOri= null;
		
	}
	
	@Override
	public Bitmap draw()
	{
		Log.d(TAG,"Drawing crawler: "+this.getId());
		sprites.loopAnim();
		return sprites.getSprite(sprites.currentFrame);
	}

	@Override
	public void HandleMessage(Telegram msg)
	{
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean isColliding(String direction)
	{
		//Log.d(TAG, "Entering overrident crawler collision");
		LevelManager lm = LevelManager.getInstance();
		TileMap map = lm.getLevel();
		//Log.d(TAG, "Crawler coord: "+TileMap.pixelsToTiles((int) this.position.x)+","+TileMap.pixelsToTiles((int) this.position.y));
		int tempX, tempY;
		
		tempX = TileMap.pixelsToTiles((int) this.position.x);
		tempY = TileMap.pixelsToTiles((int) this.position.y);
		
		if(direction == "UP")
		{
			//tempX = TileMap.pixelsToTiles((int) this.position.x + this.spriteWidth/2);
			//tempY = TileMap.pixelsToTiles((int) this.position.y - 1);
			tempY -= 1;
		}
		else if(direction == "DOWN")
		{
			//tempX = TileMap.pixelsToTiles((int) this.position.x + this.spriteWidth/2);
			//tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight + 1);
			tempY += 1;
		}
		else if(direction == "LEFT")
		{
			//tempX = TileMap.pixelsToTiles((int) this.position.x - 1);
			//tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight/2);
			tempX -= 1;
			
		}
		else if(direction == "RIGHT")
		{
			//tempX = TileMap.pixelsToTiles((int) this.position.x + this.spriteWidth + 1);
			//tempY = TileMap.pixelsToTiles((int) this.position.y + this.spriteHeight/2);
			tempX += 1;
			//tempY -= 1;
		}
		else
		{
			tempX = TileMap.pixelsToTiles((int) this.position.x );
			tempY = TileMap.pixelsToTiles((int) this.position.y );
		}
		//Log.d(TAG,"Checking for "+direction+"collision at: "+tempX+","+tempY);
		if(map.getTile(tempX, tempY) != null)
		{
			//Log.d(TAG,"Found "+direction+" Collision!!");
			return true; 
		}
		else
		{
			//Log.d(TAG,"No "+direction+" collision");
			return false;
		}
			
	}

}

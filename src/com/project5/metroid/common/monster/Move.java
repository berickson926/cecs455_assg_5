package com.project5.metroid.common.monster;

import android.graphics.Rect;
import android.util.Log;

import com.project5.metroid.common.BaseGameEntity;
import com.project5.metroid.common.EntityManager;
import com.project5.metroid.common.MessageDispatcher;
import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.message_type;
import com.project5.metroid.common.Player.Player;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;
import com.project5.metroid.common.monster.Crawler.orientation;

public class Move extends State<Crawler>
{
	public static final String TAG = "CrawlerMove";
	@Override
	public void enter(Crawler E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute(Crawler E)
	{
		//Actually move, according to AI changes
		if(E.orient == orientation.UP)
		{
			//Log.d(TAG,"Moved right");
			E.position.x += E.velocity;
		}
		else if(E.orient == orientation.LEFT)
		{
			//Log.d(TAG,"Moved up");
			E.position.y -= E.velocity;
		}
		else if(E.orient == orientation.RIGHT)
		{
			//Log.d(TAG,"Moved Down");
			E.position.y += E.velocity;
		}
		else if(E.orient == orientation.DOWN)
		{
			//Log.d(TAG, "Moved Left");
			E.position.x -= E.velocity;
		}
		else
			Log.d(TAG,"Movement Error");
		
		//check for collision with player
		Rect cRect = new Rect((int)E.position.x, (int)(E.position.y + E.spriteHeight/2), (int)E.position.x+E.spriteWidth, (int)E.position.y+E.spriteHeight);
		
		EntityManager em = EntityManager.getInstance();
		BaseGameEntity player = em.getEntityFromID(Player.playerID);
		Rect rect = new Rect((int)(player.position.x), 
							 (int)(player.position.y),
							 (int)(player.position.x+player.spriteWidth),
							 (int)(player.position.y+player.spriteHeight));
		if(Rect.intersects(cRect, rect) && E.hitPlayer == false)
		{
			//dispatch message to player
			MessageDispatcher md = MessageDispatcher.getInstance();
			
			md.dispatch(0, E, em.getEntityFromID(Player.playerID), message_type.ATTACKED, E.damage);
			E.hitPlayer = true;
		}
		else
			E.hitPlayer = false; //reset hit flag.
		
	}

	@Override
	public void exit(Crawler E)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onMessage(Crawler E, Telegram telegram)
	{
		Log.d(TAG,"Received a message!");
		if(telegram.message == message_type.ATTACKED)
		{
			Log.d(TAG,"Received attacked message!");
			E.hp -= telegram.extraInfo;
			Log.d(TAG,"HP is: "+E.hp);
			if(E.hp < 1)
			{
				LevelManager lm = LevelManager.getInstance();
				TileMap map = lm.getLevel();
				map.removeGameObject(E);
				
				//Play death animation??
			}
			
			return true;
		}
		return false;
	}

}

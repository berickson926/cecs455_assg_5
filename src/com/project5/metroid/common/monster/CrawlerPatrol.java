package com.project5.metroid.common.monster;

import com.project5.metroid.common.State;
import com.project5.metroid.common.Telegram;
import com.project5.metroid.common.monster.Crawler.orientation;

public class CrawlerPatrol extends State<Crawler>
{
	orientation orient; //keeps track of the orientation for moving the crawler around
	
	@Override
	public void enter(Crawler E)
	{
		//Assumes we always start off on flat ground, not on a wall or something
		orient = Crawler.orientation.UP;
	}

	@Override
	public void execute(Crawler E)
	{
		if(E.orient == orientation.UP)
		{
			if(E.isColliding("RIGHT"))
			{
				E.orient = orientation.LEFT;
				E.sprites.setPlayingFrames(6,1);
			}
			else if(!E.isColliding("DOWN"))
			{
				E.orient = orientation.RIGHT;
				E.prevOri= orientation.UP;
				E.sprites.setPlayingFrames(2,1);
			}
		}
		else if(E.orient == orientation.DOWN)
		{
			if(E.isColliding("LEFT"))
			{
				E.orient = orientation.RIGHT;
				E.sprites.setPlayingFrames(2, 1);
			}
			else if(!E.isColliding("UP"))
			{
				E.orient = orientation.LEFT;
				E.sprites.setPlayingFrames(6, 1);
			}
		}
		else if(E.orient == orientation.LEFT)
		{
			if(E.isColliding("UP"))
			{
				E.orient = orientation.UP;
				E.sprites.setPlayingFrames(0,1);
			}
			else if(!E.isColliding("RIGHT"))
			{
				E.orient = orientation.DOWN;
				E.sprites.setPlayingFrames(4, 1);
			}
		}
		else if(E.orient == orientation.RIGHT)
		{
			if(E.isColliding("DOWN"))
			{
				E.orient = orientation.UP;
				E.sprites.setPlayingFrames(0,1);
			}
			else if(!E.isColliding("LEFT"))
			{
				if(E.prevOri != orientation.UP)
				{
					E.orient = orientation.DOWN;
					E.sprites.setPlayingFrames(4, 1);
				}
			}
			else
				E.prevOri = orientation.RIGHT;
		}
	}

	@Override
	public void exit(Crawler E)
	{
		
	}

	@Override
	public boolean onMessage(Crawler E, Telegram telegram)
	{
		// TODO Auto-generated method stub
		return false;
	}

}

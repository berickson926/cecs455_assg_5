package com.project5.metroid.common;

public enum message_type
{
	ATTACKED,ATTACK,ATTACK_UP,
	
	MOVELEFT,MOVELEFT_ATTACK,MOVELEFT_ATTACK_UP,MOVELEFT_AIM_UP,
	
	MOVERIGHT,MOVERIGHT_ATTACK,MOVERIGHT_ATTACK_UP,MOVERIGHT_AIM_UP,
	
	JUMPRIGHT,JUMPLEFT,JUMP,
	
	FALLLEFT,FALLRIGHT,FALLATTACKLEFT,FALLATTACKRIGHT,
	
	STANDLEFT,STANDRIGHT,NOACTION,AIM_UP,
	
	DUCK,DUCKLEFT,DUCKRIGHT,
	
	
	//powerup messages
	BALL_ACQUIRED,
	HEALTH_ACQUIRED,
	MISSLE_ACQUIRED;
	
}

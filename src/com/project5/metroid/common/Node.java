package com.project5.metroid.common;

public class Node
{
	Node	NW;
	Node 	NE;
	Node 	SW;
	Node 	SE;
	
	int		depth;
	boolean	isLeaf;
	int		type;
	int		size;
	int		x;
	int		y;

	
	Node(int parentDepth)
	{
		depth = parentDepth + 1;
		if (depth > 5)
		{
			NW = null;
			NE = null;
			SW = null;
			SE = null;
			isLeaf = true;
			type = 0;
		}
		else
		{			
			NW = new Node(depth);
			NE = new Node(depth);
			SW = new Node(depth);
			SE = new Node(depth);
			isLeaf = false;
			type = -1;
		}
	}
	
	public void readtree()
	{
		if (isLeaf)
			System.out.println("NODELEAF");
		else
		{
			NW.readtree();
			NE.readtree();
			SW.readtree();
			SE.readtree();
		}
	}
}

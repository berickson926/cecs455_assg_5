package com.project5.metroid;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.project5.metroid.common.gamepad.GamePad;

/**
 * 
 * @author bret
 *
 */
public class GameActivity extends Activity
{
	//Debugging
	public static final String TAG = "METROID_GAME_ACTIVITY";
	
	public static MediaPlayer music;
	public static MediaPlayer fx;
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		//Force focus into a full screen state, remove status bars, etc & keep landscape mode
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(new MainGamePanel(this));
		
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        GameActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        //DEBUG: Testing song transitions. Eventually migrate music handling to maingamepanel
        setUpTitleScreenMusic();//start music
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		music.stop();
	}
	
	@Override
	public void onRestart()
	{
		super.onRestart();
		setUpTitleScreenMusic();
	}
	
	@Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
    	//Ignore orientation change to keep activity from restarting
    	super.onConfigurationChanged(newConfig);
    }
	
	public void setUpTitleScreenMusic()
    {
    	//Setup title screen music
        music = MediaPlayer.create(GameActivity.this, R.raw.metroid03);
        music.start();
        music.setLooping(true);
        music.setVolume(2,2);
    }
}

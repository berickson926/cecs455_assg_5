package com.project5.metroid;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

public class MainMenuActivity extends Activity implements OnClickListener 
{
	public static final String TAG = "METROID_MAIN_MENU";//Debugging; use: Log.d(TAG,"text");
	
	private MediaPlayer music;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	super.onCreate(savedInstanceState);
    	
    	//Set up the visuals/music/touch listeners
    	setUpTitleScreen();
    }
    
    @Override
    public void onPause()
    {
    	super.onPause();
    	music.stop();
    }
    
    @Override
    public void onRestart()
    {
    	super.onRestart();
    	music.reset();
    	setUpTitleScreenMusic();
    }
    
    @Override
    public void onDestroy()
    {
    	Log.d(TAG, "Attempting to destroy");
    	//music.stop();
    	//music.release();
    	Log.d(TAG,"Released MainMenuActivityMusic");
    	super.onDestroy();
    	
    }
    
    @Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
    	//Ignore orientation change to keep activity from restarting
    	super.onConfigurationChanged(newConfig);
    }
    
    public void setUpTitleScreen()
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        MainMenuActivity.this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    	
    	//setup touch listener
    	View startButton = findViewById(R.id.metroid_start_button);
        startButton.setOnClickListener(this);
        
        //Once title screen is set up, initialize title screen music
    	setUpTitleScreenMusic();
    }
    
    public void setUpTitleScreenMusic()
    {
    	//Setup title screen music
        music = MediaPlayer.create(MainMenuActivity.this, R.raw.metroid01);
        music.start();
        music.setLooping(true);
        music.setVolume(10,10);
    }

	@Override
	public void onClick(View v)
	{
		//On touch we start up the game
		if(v.getId() == R.id.metroid_start_button)
		{				
			music.stop();
			Log.d(TAG,"Detected start game button click");
			Intent newGameIntent = new Intent(this, GameActivity.class);
			startActivity(newGameIntent);	
		}
	}
    
}
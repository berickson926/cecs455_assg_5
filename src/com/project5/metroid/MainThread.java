package com.project5.metroid;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Main thread which contains the game loop. 
 * 
 * @author bret
 *
 */
public class MainThread extends Thread
{
	private static final String TAG = MainThread.class.getSimpleName();
	
	private SurfaceHolder surfaceHolder;
	
	//The actual view that handles input
	//and draws to the surface
	private MainGamePanel gamePanel;
	
	//desired FPS
	private final static int MAX_FPS = 50;
	//maximum number of rames to be skipped
	private final static int MAX_FRAME_SKIPS = 5;
	//the frame period
	private final static int FRAME_PERIOD = 1000 / MAX_FPS;
	
	private boolean running;//flag to hold game state
	public void setRunning(boolean running)
	{
		this.running = running;
	}
	
	public MainThread(SurfaceHolder surfaceHolder, MainGamePanel gamePanel)
	{
		super();
		this.surfaceHolder = surfaceHolder;
		this.gamePanel = gamePanel;
		
	}
	
	@Override
	public void run()
	{
		Canvas canvas;
		
		long beginTime;		//the time when cycle has begun
		long timeDiff;		//the time it took for the cycle to execute
		int sleepTime;		//ms to sleep(<0 if we're behind)
		int framesSkipped;	//number of frames being skipped
		
		sleepTime = 0;
		
		while(running)
		{
			canvas = null;
			//try locking the canvas for exclusive pixel editing in the surface
			
			try
			{
				canvas = this.surfaceHolder.lockCanvas();
				
				synchronized(surfaceHolder)
				{
					beginTime = System.currentTimeMillis();
					framesSkipped = 0; //Resetting the frames skipped
					
					//update the game state
					this.gamePanel.update();
					//render the state to the screen
					//draws the canvas on the panel
					this.gamePanel.render(canvas);
					
					
					//calculate how long did the cycle take
					timeDiff = System.currentTimeMillis() - beginTime;
					//calculate sleep time
					sleepTime = (int)(FRAME_PERIOD - timeDiff);
					
					if(sleepTime > 0)
					{
						//if sleepTime > 0 we're OK
						try
						{
							//send the thread to sleep for a short period
							Thread.sleep(sleepTime);
						}
						catch(InterruptedException e){}
					}//end if
					
					while(sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS)
					{
						//need to catch up, update without rendering
						
						//add frame period to check if in next frame
						sleepTime += FRAME_PERIOD;
						framesSkipped++;
					}//end while
				}
			}
			finally
			{
				//in case of an exception the surface is not left in an inconsistent state
				if(canvas != null)
					surfaceHolder.unlockCanvasAndPost(canvas);
			}//end finally
		}
	}
	
}

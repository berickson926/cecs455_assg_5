package com.project5.metroid;

import android.app.Activity;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.project5.metroid.common.EntityManager;
import com.project5.metroid.common.MessageDispatcher;
import com.project5.metroid.common.Player.Player;
import com.project5.metroid.common.environment.LevelManager;
import com.project5.metroid.common.environment.TileMap;
import com.project5.metroid.common.gamepad.GamePad;

public class MainGamePanel extends SurfaceView implements SurfaceHolder.Callback
{
	//For logcat debug
	private static final String TAG = "MAIN_GAME_PANEL";
	
	
	private MainThread thread;
	
	//Game engine resources
	public Player player;
	private GamePad gamePad;
	private EntityManager entityManager;
	private MessageDispatcher messageDispatcher;
	private LevelManager levelManager;
	
	//Debugging the game grid
	public TileMap tileMap;
	
	public MainGamePanel(Context context)
	{
		super(context);
		
		//adding the callback(this) to the surface holder to intercept events
		getHolder().addCallback(this);
		
		//create player and load bitmap
		Log.d(TAG,"Creating player obj");
		player = new Player(this.getContext());//this.getContext required to fetch bitmap resources?
		Log.d(TAG, "Created player obj. with ID: "+player.getId());
		
		//create the game loop thread
		Log.d(TAG, "Starting game loop");
		thread = new MainThread(getHolder(), this);
		
		//make the gamepanel focusable so it can handle events
		setFocusable(true);
		setFocusableInTouchMode(true);	
	}//End constructor

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		//Initialize Game resources. We have to wait until surface is created, otherwise the
		//canvas will not be defined. It is necessary to have canvas created for gamegrid setup
		initializeResources();
				
		//at this point the surface is created and we can safely start the game loop
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		Log.d(TAG, "The surface is being destroyed");
		//Tell the thread to shut down and wait for it to finish
		//this is a clean shutdown
		boolean retry = true;
		while(retry)
		{
			try
			{
				thread.setRunning(false);
				thread.join();
				((Activity)getContext()).finish();
				retry = false;
			}
			catch(InterruptedException e)
			{
				//try again shutting down the thread
			}
		}
		
		Log.d(TAG, "Thread was shut down cleanly.");
		
	}//end surfaceDestroyed
	
	
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		//Let the Gamepad overlay figure out touch events for controlling the player
		if(gamePad.handleInput(event))
			return true;  //event was handled by gamepad
		else
			return false; //Event wasn't handled.
		
	}//end onTouchEvent
	
	public void render(Canvas canvas)
	{
		//Draw background
		canvas.drawColor(Color.BLACK);
	
		//Draw all relevant graphical info
		tileMap.draw(canvas); 		//Player is now drawn from the map call
		gamePad.draw(canvas);		//Draws the gamePad overlay
		drawPlayerStatus(canvas);	//Draws UI relating to the players' status, ie. energy, missles, etc.
	}

	public void update()
	{
		//Allow all objects in game engine to update their state machines accordingly
		gamePad.update(player);
		//player.update(); Moved into tileMap
		tileMap.update();
		
	}
	
	/**
	 * Draws the player status info to the UI
	 * Includes: Energy level
	 * 
	 * Future additions:
	 * Energy Tank icons
	 * Missle qty status
	 * 
	 * @param canvas
	 */
	public void drawPlayerStatus(Canvas canvas)
	{
		//Setup
		Paint paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		paint.setAntiAlias(true);
		paint.setFakeBoldText(true);
		paint.setShadowLayer(5, 5, 5, Color.BLACK);
		
		//Paint the text info
		canvas.drawText("EN.."+player.getEnergy(), 20, 30, paint);
	}
	
	public void initializeResources()
	{
		entityManager = new EntityManager();			//
		messageDispatcher = new MessageDispatcher();	//handles message exchange between basegameobjects
		gamePad = new GamePad(this.getContext());		//constructs the gamepad ui and handles touch events
		
		//level manager loads info from xml file into memory, then manages the game grid & other level info
		Log.d(TAG,"Initializing XML parse");
		XmlResourceParser file = getResources().getXml(com.project5.metroid.R.xml.level_source);
		levelManager = new LevelManager(getHolder().getSurfaceFrame());				
		levelManager.loadLevels(file);
		file.close();
		
		
		//DEBUG:Gamegrid testing
		Log.d(TAG, "Initializing tilemap");
		
		tileMap = new TileMap(this.getContext());
		Log.d(TAG,"Player being placed on map at: "+player.getPosition().getX()+","+player.getPosition().getY());
		tileMap.addGameObject(player, player.getPosition().getX(), player.getPosition().getY());
		Log.d(TAG,"Adding tilemap to levelmanager maplist: "+levelManager.levelList.size());
		LevelManager.getInstance().addLevel(tileMap);
		//levelManager.addLevel(tileMap);
		Log.d(TAG,"Level added. : "+levelManager.levelList.size());
	}
}
